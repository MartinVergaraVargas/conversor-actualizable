package com.example.conversor_de_moneda_lab1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    public void onClick_boton_conversor(View view){

        final EditText ingreso = (EditText) findViewById(R.id.editText_ingreso);
        final Spinner selector = (Spinner) findViewById(R.id.selector);
        final TextView conversion = (TextView) findViewById(R.id.textView_conversion);

        String seleccion = selector.getSelectedItem().toString();
        Float intIngreso = Float.parseFloat(ingreso.getText().toString());
        Toast.makeText(view.getContext(), seleccion , Toast.LENGTH_SHORT).show();

        if(seleccion.equals("USD a CLP")){

            float UaC = (intIngreso * 677.5f);
            //UaC = Math.round(UaC);
            conversion.setText("CLP $" + UaC);
        }

        if(seleccion.equals("CLP a USD")){

            float CaU = (intIngreso / 677.5f);
            //CaU = Math.round(CaU);
            conversion.setText("USD $" + CaU);
        }

    }






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    //private class JsonTask extends AsyncTask<String, String, String> {

      //  protected void onPreExecute() {
        //    super.onPreExecute();

          //  pd = new ProgressDialog(MainActivity.this);
          //  pd.setMessage("Please wait");
          //  pd.setCancelable(false);
          //  pd.show();
       // }

        //protected String doInBackground(String... params) {


            //HttpURLConnection connection = null;
            //BufferedReader reader = null;

            //try {
              //  URL url = new URL(params[0]);
                //connection = (HttpURLConnection) url.openConnection();
                //connection.connect();


               // InputStream stream = connection.getInputStream();

                //reader = new BufferedReader(new InputStreamReader(stream));

                //StringBuffer buffer = new StringBuffer();
                //String line = "";

                //while ((line = reader.readLine()) != null) {
                  //  buffer.append(line+"\n");
                   // Log.d("Response: ", "> " + line);   //here u ll get whole response...... :-)

                //}

                //return buffer.toString();


            //} catch (MalformedURLException e) {
              //  e.printStackTrace();
            //} catch (IOException e) {
              //  e.printStackTrace();
            //} finally {
             //   if (connection != null) {
               //     connection.disconnect();
               // }
                //try {
                  //  if (reader != null) {
                    //    reader.close();
                   // }
               // } catch (IOException e) {
                 //   e.printStackTrace();
                //}
           // }
            //return null;
        //}

        //@Override
        //protected void onPostExecute(String result) {
          //  super.onPostExecute(result);
          //  if (pd.isShowing()){
          //      pd.dismiss();
          //  }
           // resultado.setText(result);
       // }
    //}

}
